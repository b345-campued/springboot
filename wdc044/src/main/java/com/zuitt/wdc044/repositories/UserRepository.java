package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// an interface contains behaviour that a class implements
// an interface marked as @Repository contains methods for database manipulation
// by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating,and deleting
@Repository
public interface UserRepository extends CrudRepository<User, Object>{
    //https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    //Username query should be existent in user table columns
    //findBy<query> is a form of modification of default which is Id
    User findByUsername(String username);
}
