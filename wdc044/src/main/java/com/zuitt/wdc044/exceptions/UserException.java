package com.zuitt.wdc044.exceptions;

public class UserException extends Exception {

    //This method will just capture the exception
    // With the super method it will inherit parent class properties and methods
    public UserException(String message){
        super(message);
    }
}
