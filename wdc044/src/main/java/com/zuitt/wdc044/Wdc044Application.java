package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


// "@SpringBootApplication" is an example of "Annotation" mark.
	// Annotations are used to provide supplemental information about the program.
	// These are used to manage and configure the behavior of the framework.
	// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
	// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.


//This specifies the main class of the spring boot application.
@SpringBootApplication

// This indicates that a class is a controller that will handle RESTful web requests and returns an HTTP response.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		//This method starts the whole Spring Framework
		//This serves as entry point to start the application
		SpringApplication.run(Wdc044Application.class, args);
	}

	// This is used to map HTTP GET requests.
		// Note: HTTP Request annotations is usually followed by a "method body"
		// The "method body" contains the logic to generate the response.

	//No ; since we chain annotation with the method
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters, form parameters, and even files from the request.
		// if the URL is: localhost:8080/hello?name=john the method will return "Hello john"
			// "?" means that the start of the parameters followed by the "key=value" pair.
		// if the URL is: localhost:8080/hello, the method will return "Hello World."
		//String name here is the binding variable to hold the query
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		// To send a response of "Hello + name"
		// %s is placeholder value for the name
		return String.format("Hello %s", name);
	}

	//	Activity s01
		// localhost:8080/hi?user=jane
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("hi %s!", user);
	}

	//	Activity Stretch Goal:
		// localhost:8080/nameAge?user=franz&age=26
	@GetMapping("/nameAge")
		//	"&" is used to handle multiple parameters.
		//	"%s" and "%d" is called as "Format Specifier"
	public String nameAge(@RequestParam(value = "name", defaultValue = "user") String name, @RequestParam(value = "age", defaultValue = "0") int age){
		return String.format("Hello %s! Your age is %d.", name, age);
	}

}


//NOTES:

// a. Creating a spring boot project
	// Go to https://start.spring.io/ to initialize a spring boot project
		// Set up the following:
			// Project: Maven
			// Language: Java
			// Spring Boot: 2.7.14 (or the most previous update)
			// Group: com.nameOfThePackage
			// Artifact & Name: projectName
			// Packaging: War (for creating web apps)
			// Java: 11 (jdk version)
			// Under Dependencies: Click Add Dependencies > Spring Web
		// Click Generate
//b. Extract the Zip file and open the file in intellij
//c. Look for the pom.xml and do the following:
	// Right Click pom.xml > Look for Maven > Download Sources > Wait for the  download to be done.
	// Right Click again pom.xml > Maven >  Reload Project > This should fix any errors on first time of opening the project.
		// Note: the step "c" should also be followed if new dependencies is added on the pom.xml

// command for running the application:
	// ./mvnw spring-boot:run
		// Note: This will also allow us to serve the Spring boot project on tomcat server.
