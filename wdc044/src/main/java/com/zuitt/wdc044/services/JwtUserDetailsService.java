package com.zuitt.wdc044.services;


// We need to have access directly to the users table through users model
import com.zuitt.wdc044.models.User;
//We need to have access to UserRepository because we will need the methods inside reporsitory
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
// Service
import org.springframework.security.core.userdetails.UserDetails;
//Sercive implementation
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
// The component annotation inform/declare our class is a component class
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    // Composition- we create  instance inside class. One property is instance of a class
    // Autowired means that instance of a class that implements
    // Dependency injection
    @Autowired
    private UserRepository userRepository;

    @Override
    // public UserDetails (data type to return)
    // loadUserByUsername is method from UserDetailsService
    // throws is for exception
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

        //This serves as creation of access token
        // No data type in ArrayList<>() since we do not know it it will return and contain a value
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

}

