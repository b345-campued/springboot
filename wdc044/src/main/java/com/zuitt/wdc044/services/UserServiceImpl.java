package com.zuitt.wdc044.services;


import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

//indicate that we are going to implement a service by using Service annotation
@Service
public class UserServiceImpl implements UserService{

    //Composition- we create  instance inside class. One property is instance of a class
    // Here we use UserRepository
    // Dependency injection
    // Autowired means that instance of a class that implements
    // When it comes to the userRepository property, it will contain an instance of UserRepository Class.
    @Autowired
    private UserRepository userRepository;

    public void createUser(User user){
        //save user in the table
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        //Null can return as value if user record cannot be found
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

}
