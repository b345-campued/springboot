package com.zuitt.wdc044.services;

//import user model
import com.zuitt.wdc044.models.User;
//
import java.util.Optional;
public interface UserService {

    //To create default service/access modifier
    void createUser(User user);

    //We create a function that return Optional object
    //Return whole document/record that match the username provided; if none Optional will return null
    //default security access modifier
    Optional<User> findByUsername(String username);

}
