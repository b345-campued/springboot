package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;
public interface PostService {

    //We need token for authorization/authentication
    void createPost(String stringToken, Post post);
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    ResponseEntity deletePost(Long id, String stringToken);
    Iterable<Post> getAllMyPost(String stringToken);
    Iterable<Post> getPosts();
}

